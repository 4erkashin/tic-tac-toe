'use strict'

/* Симуляция выбора стороны игроком */
const userSymbol = 'x';
const aiSymbol = 'o';

/* Создание игрового поля из 9 элементов и запись в cells их HTMLCollection для последующей работы с DOM */
const cells = createBoard(9);

/* Определение массива всех возможных выигрышных комбинаций */
const winComb = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

function GameBot() {}
GameBot.prototype.center = cells[4];
GameBot.prototype.corners = [cells[0], cells[2], cells[6], cells[8]];

/* Сценарии поведения бота в зависимости от ситуации на доске */
GameBot.prototype._behaviors = {
  hitCenter: (center) => {
    center.innerHTML = aiSymbol;
    updateWinComb(center, aiSymbol);
    checkWin(winComb, aiSymbol);
  },
  hitCorner: (corner) => {
    corner.innerHTML = aiSymbol;
    updateWinComb(corner, aiSymbol);
    checkWin(winComb, aiSymbol);
  },
  hitFreeCell: (cell) => {
    cell.innerHTML = aiSymbol;
    updateWinComb(cell, aiSymbol);
    checkWin(winComb, aiSymbol);
  },
  hitToWin: (selfWinIndex) => {
    cells[selfWinIndex].innerHTML = aiSymbol;
    updateWinComb(cells[selfWinIndex], aiSymbol);
    checkWin(winComb, aiSymbol);
  },
  hitToDontLose: (enemyWinIndex) => {
    cells[enemyWinIndex].innerHTML = aiSymbol;
    updateWinComb(cells[enemyWinIndex], aiSymbol);
    checkWin(winComb, aiSymbol);
  }
}

/* Условия выбора конкретного сценария поведения */
GameBot.prototype._resolveBehavior = function (params) {
  if (params.selfWinIndex !== null) {
    return this._behaviors.hitToWin.bind(this, params.selfWinIndex);
  }
  if (params.enemyWinIndex !== null) {
    return this._behaviors.hitToDontLose.bind(this, params.enemyWinIndex);
  }
  if (this.center.innerHTML === '') {
    return this._behaviors.hitCenter.bind(this, this.center);
  }
  if (params.freeCorners.length !== 0) {
    return this._behaviors.hitCorner.bind(this, params.corner);
  }
  if (params.freeCells.length !== 0) {
    return this._behaviors.hitFreeCell.bind(this, params.cell);
  }
}

/* Метод с ходом бота */
GameBot.prototype.move = function () {
  let freeCorners = checkFreeCells(this.corners);
  let freeCells = checkFreeCells(cells);
  let corner = getRandomIndex(freeCorners);
  let cell = getRandomIndex(freeCells);
  let selfWinIndex = preWin(winComb, aiSymbol);
  let enemyWinIndex = preWin(winComb, userSymbol);
  let params = {
    freeCorners: freeCorners,
    freeCells: freeCells,
    corner: corner,
    cell: cell,
    selfWinIndex: selfWinIndex,
    enemyWinIndex: enemyWinIndex
  };
  let behaviorMethod = this._resolveBehavior(params);
  behaviorMethod();
}

/* Заплатка для показа, что логика игры бота работает
сделать полный игровый процесс по ТЗ я не успел :( */
let bot = new GameBot();
if (aiSymbol === 'x') {
  bot.move();
} else {
  alert('User turn');
}

/* Функция создания игрового поля */
function createBoard(cellsAmount) {
  let board = document.createElement('div');
  board.classList.add('grid-container');
  document.body.prepend(board);

  for (let i = 0; i < cellsAmount; i++) {
    let cell = document.createElement('div');
    cell.classList.add('grid-container__item');
    cell.dataset.index = i;
    /* Задаю каждой игровой клетке атрибут data-index, для того, чтобы взаимодействовать с выигрышными комбинациями */
    cell.addEventListener('click', makeUserMove);
    board.append(cell);
  }
  return document.getElementsByClassName('grid-container__item');
  /* Возвращаю HTML коллекцию игровых клеток */
}

function makeUserMove() {
  if (this.innerHTML !== '') {
    alert('Already taken');
  } else {
    this.innerHTML = userSymbol;
    updateWinComb(this, userSymbol);
    checkWin(winComb, userSymbol);
    bot.move();
  }
}

function updateWinComb(cell, symbol) {
  for (let i = 0; i < winComb.length; i++) {
    let index = winComb[i].indexOf(Number(cell.dataset.index));
    if (index !== -1) {
      winComb[i][index] = symbol;
    }
  }
}
/* Обновляю массивы выигрышных комбинаций путём нахождения всех вхождений значений data-index
той клетки игрового поля, в которую был совершен ход */

function checkWin(arr, symbol) {
  if (checkFreeCells(cells).length === 0) {
    setTimeout(() => {
      alert('There is draw');
    }, 150);
  } else {
    for (let i = 0; i < arr.length; i++) {
      let storage = [];
      let index = arr[i].indexOf(symbol);
      while (index !== -1) {
        storage.push(index);
        index = arr[i].indexOf(symbol, index + 1);
        if (storage.length === 3) {
          setTimeout(() => {
            alert('Win condition reached');
          }, 150);
        }
      }
    }
  }
}
/* Проверяю на наличие выигрышной комбинации с тремя одинаковыми символами, чтобы зафиксировать победу,
а если свободных для игры клеток нет — фиксирую ничью */

function preWin(arr, symbol) {
  for (let i = 0; i < arr.length; i++) {
    let storage = [];
    let index = arr[i].indexOf(symbol);
    while (index !== -1) {
      storage.push(index);
      index = arr[i].indexOf(symbol, index + 1);
      if (storage.length === 2) {
        for (let j = 0; j < arr[i].length; j++) {
          if ((arr[i][j] !== symbol) && (typeof (arr[i][j]) === 'number')) {
            return arr[i][j];
          }
        }
      }
    }
  }
  return null;
}
/* Проверяю ситуацию когда в выигрышной комбинации есть два одинаковых игровых символа
и возвращаю data-index той клетки, в которую нужно сходить */

function checkFreeCells([...arr]) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].innerHTML !== '') {
      arr.splice(i, 1);
      i--;
    }
  }
  return arr;
}
/* Проверяю на наличие свободных игровых клеток
тут кстати применяю костыль в виде спред-оператора для преобразования HTMLCollection к массиву
пробовал и Array.of, и [].splisce.call(args), но возникали ошибки с которыми я хотел разобраться позже
и по классике не успел :( */

function getRandomIndex(arr) {
  let min = 0;
  let max = arr.length - 1;
  let rand = min + Math.floor(Math.random() * (max + 1 - min));
  return arr[rand];
}
/* Если основные условия не выполняются, то тут я выбираю рандомную клетку для хода бота */